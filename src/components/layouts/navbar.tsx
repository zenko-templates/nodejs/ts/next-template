import {
    AppBar,
    Box,
    Toolbar,
    Typography
} from '@mui/material';
import type { NextPage } from 'next'

const NavBar: NextPage = () => {

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div">
                        {/* eslint-disable-next-line react/no-unescaped-entities */}
                        Zenko's Template
                    </Typography>
                </Toolbar>
            </AppBar>
        </Box>
    )
}

export default NavBar
