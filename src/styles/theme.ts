import { createTheme } from '@mui/material/styles';

declare module '@mui/material/styles' {
	interface Theme {

	}
	// allow configuration using `createTheme`
	interface ThemeOptions {

	}
}

// Create a theme instance.
const theme = createTheme({

});

export default theme;
