import {combineReducers, configureStore} from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import {generated, productApi, categoryApi} from "../api";


const rootReducer = combineReducers({
	// You can add reducer here
	[generated.api.reducerPath]: generated.api.reducer,
	[productApi.reducerPath]: productApi.reducer,
	[categoryApi.reducerPath]: categoryApi.reducer,
});

export const store = configureStore({
	reducer: rootReducer,
	middleware: getDefaultMiddleware => [
		// You can add middleware here for async method
		...getDefaultMiddleware(),
		generated.api.middleware,
		productApi.middleware,
		categoryApi.middleware,
	],
});

setupListeners(store.dispatch);