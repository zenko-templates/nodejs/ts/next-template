import '../styles/globals.css'
import type {AppProps} from 'next/app'
import Head from "next/head";
import { Provider } from 'react-redux'
import {store} from "../stores/store";
import {ThemeProvider} from "@mui/material";
import theme from "../styles/theme";
import { SnackbarProvider } from 'notistack';
import Layout from "../components/layouts/layout";

function MyApp({Component, pageProps}: AppProps) {
	return (
		<>
			<Head>
				<title>Zenko Nextjs Template</title>
				<meta name="description" content="Zenko's Nextjs template"/>
				<link rel="icon" href="/favicon.ico"/>
			</Head>
			<Provider store={store}>
				<ThemeProvider theme={theme}>
					<SnackbarProvider
						maxSnack={3}
						anchorOrigin={{
						vertical: 'bottom',
						horizontal: 'right',
					}}
					>
						<Layout>
							<Component {...pageProps} />
						</Layout>
					</SnackbarProvider>
				</ThemeProvider>
			</Provider>
		</>
	)
}

export default MyApp
