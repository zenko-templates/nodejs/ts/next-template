import { api as generatedApi } from '../generated'

export const categoryApi = generatedApi.enhanceEndpoints({
	addTagTypes: ['Categories', 'Category'],
	endpoints: {
		getCategory: {
			providesTags: (result, error, arg) => [{ type: 'Category', id: arg.id}]
		},
		getCategories: {
			providesTags: (result) => result
				? [
					// Provides a tag for each post in the current page,
					// as well as the 'PARTIAL-LIST' tag.
					...result.categories.edges.map(( category ) => ({ type: 'Categories' as const, id: category.node.id })),
					{ type: 'Categories', id: 'PARTIAL-LIST' },
				]
				: [{ type: 'Categories', id: 'PARTIAL-LIST' }],
		},
		createCategory: {
			invalidatesTags: (result) => result
				? [
					{ type: 'Categories', id: result.createCategory.id },
					{ type: 'Categories', id: 'PARTIAL-LIST' },
				]
				: [{ type: 'Categories', id: 'PARTIAL-LIST' }],
		},
		updateCategory: {
			invalidatesTags: (result, error, arg) => result
				? [
					{ type: 'Category', id: arg.input.id },
					{ type: 'Categories', id: arg.input.id },
					{ type: 'Categories', id: 'PARTIAL-LIST' },
				]
				: [{ type: 'Categories', id: 'PARTIAL-LIST' }],
		},
		deleteCategory: {
			invalidatesTags: (result, error, arg) => result
				? [
					{ type: 'Categories', id: arg.id },
					{ type: 'Categories', id: 'PARTIAL-LIST' },
				]
				: [{ type: 'Categories', id: 'PARTIAL-LIST' }],
		},
	}
})

export const {
	useGetCategoryQuery,
	useLazyGetCategoryQuery,
	useGetCategoriesQuery,
	useLazyGetCategoriesQuery,
	useCreateCategoryMutation,
	useUpdateCategoryMutation,
	useDeleteCategoryMutation,
} = categoryApi