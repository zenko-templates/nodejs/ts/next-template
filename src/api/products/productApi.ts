import { api as generatedApi } from '../generated'

export const productApi = generatedApi.enhanceEndpoints({
	addTagTypes: ['Products', 'Product'],
	endpoints: {
		getProduct: {
			providesTags: (result, error, arg) => [{ type: 'Product', id: arg.id}]
		},
		getProducts: {
			providesTags: (result) => result
				? [
					// Provides a tag for each post in the current page,
					// as well as the 'PARTIAL-LIST' tag.
					...result.products.edges.map(( product ) => ({ type: 'Products' as const, id: product.node.id })),
					{ type: 'Products', id: 'PARTIAL-LIST' },
				]
				: [{ type: 'Products', id: 'PARTIAL-LIST' }],
		},
		createProduct: {
			invalidatesTags: (result) => result
				? [
					{ type: 'Products', id: result.createProduct.id },
					{ type: 'Products', id: 'PARTIAL-LIST' },
				]
				: [{ type: 'Products', id: 'PARTIAL-LIST' }],
		},
		updateProduct: {
			invalidatesTags: (result, error, arg) => result
				? [
					{ type: 'Product', id: arg.input.id },
					{ type: 'Products', id: arg.input.id },
					{ type: 'Products', id: 'PARTIAL-LIST' },
				]
				: [{ type: 'Products', id: 'PARTIAL-LIST' }],
		},
		deleteProduct: {
			invalidatesTags: (result, error, arg) => result
				? [
					{ type: 'Products', id: arg.id },
					{ type: 'Products', id: 'PARTIAL-LIST' },
				]
				: [{ type: 'Products', id: 'PARTIAL-LIST' }],
		},
	},
})

export const {
	useGetProductQuery,
	useLazyGetProductQuery,
	useGetProductsQuery,
	useLazyGetProductsQuery,
	useCreateProductMutation,
	useUpdateProductMutation,
	useDeleteProductMutation,
} = productApi