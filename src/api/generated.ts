import { api } from './baseApi';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A cursor used in the pagination */
  Cursor: string;
  /** An ISO-8601 encoded UTC date string. */
  DateTime: Date;
};

export type CategoriesInput = {
  page: PageInput;
};

export type Category = {
  __typename?: 'Category';
  id: Scalars['ID'];
  name: Scalars['String'];
  products: Array<Product>;
};

export type CategoryConnection = {
  __typename?: 'CategoryConnection';
  edges: Array<CategoryEdge>;
  pageInfo: PageInfo;
};

export type CategoryEdge = {
  __typename?: 'CategoryEdge';
  cursor: Scalars['Cursor'];
  node: Category;
};

export type CreateCategoryInput = {
  name: Scalars['String'];
};

export type CreateProductInput = {
  categoryId: Scalars['ID'];
  name: Scalars['String'];
};

/** The root mutation. */
export type Mutation = {
  __typename?: 'Mutation';
  createCategory: Category;
  createProduct: Product;
  deleteCategory: Category;
  deleteProduct: Product;
  updateCategory: Category;
  updateProduct: Product;
};


/** The root mutation. */
export type MutationCreateCategoryArgs = {
  input: CreateCategoryInput;
};


/** The root mutation. */
export type MutationCreateProductArgs = {
  input: CreateProductInput;
};


/** The root mutation. */
export type MutationDeleteCategoryArgs = {
  id: Scalars['ID'];
};


/** The root mutation. */
export type MutationDeleteProductArgs = {
  id: Scalars['ID'];
};


/** The root mutation. */
export type MutationUpdateCategoryArgs = {
  input: UpdateCategoryInput;
};


/** The root mutation. */
export type MutationUpdateProductArgs = {
  input: UpdateProductInput;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** Cursor of the last item in the results */
  endCursor?: Maybe<Scalars['Cursor']>;
  /** Does it have a next page when paginate forward */
  hasNextPage: Scalars['Boolean'];
  /** Does it have a previous page when paginate backward */
  hasPreviousPage: Scalars['Boolean'];
  /** Cursor of the first item in the results */
  startCursor?: Maybe<Scalars['Cursor']>;
};

export type PageInput = {
  /** Cursor used when paginated forward */
  after?: InputMaybe<Scalars['Cursor']>;
  /** Cursor used when paginate backward */
  before?: InputMaybe<Scalars['Cursor']>;
  /** Number of result wanted when paginate forward from the after cursor */
  first?: InputMaybe<Scalars['Int']>;
  /** Number of result wanted when paginate backward from the before cursor */
  last?: InputMaybe<Scalars['Int']>;
};

/** Input for a period search */
export type PeriodSearchInput = {
  /** End date of the period to query */
  end?: InputMaybe<Scalars['DateTime']>;
  /** Start date of the period to query */
  start?: InputMaybe<Scalars['DateTime']>;
};

export type Product = {
  __typename?: 'Product';
  category: Category;
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type ProductConnection = {
  __typename?: 'ProductConnection';
  edges: Array<ProductEdge>;
  pageInfo: PageInfo;
};

export type ProductEdge = {
  __typename?: 'ProductEdge';
  cursor: Scalars['Cursor'];
  node: Product;
};

export type ProductsInput = {
  page: PageInput;
};

/** The root query. */
export type Query = {
  __typename?: 'Query';
  categories: CategoryConnection;
  category?: Maybe<Category>;
  product?: Maybe<Product>;
  products: ProductConnection;
};


/** The root query. */
export type QueryCategoriesArgs = {
  input: CategoriesInput;
};


/** The root query. */
export type QueryCategoryArgs = {
  id: Scalars['ID'];
};


/** The root query. */
export type QueryProductArgs = {
  id: Scalars['ID'];
};


/** The root query. */
export type QueryProductsArgs = {
  input: ProductsInput;
};

export type UpdateCategoryInput = {
  id: Scalars['ID'];
  name?: InputMaybe<Scalars['String']>;
};

export type UpdateProductInput = {
  categoryId: Scalars['ID'];
  id: Scalars['ID'];
  name?: InputMaybe<Scalars['String']>;
};

export type CreateCategoryMutationVariables = Exact<{
  input: CreateCategoryInput;
}>;


export type CreateCategoryMutation = { __typename?: 'Mutation', createCategory: { __typename?: 'Category', id: string, name: string, products: Array<{ __typename?: 'Product', id: string, name: string }> } };

export type DeleteCategoryMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteCategoryMutation = { __typename?: 'Mutation', deleteCategory: { __typename?: 'Category', id: string, name: string, products: Array<{ __typename?: 'Product', id: string, name: string }> } };

export type UpdateCategoryMutationVariables = Exact<{
  input: UpdateCategoryInput;
}>;


export type UpdateCategoryMutation = { __typename?: 'Mutation', updateCategory: { __typename?: 'Category', id: string, name: string, products: Array<{ __typename?: 'Product', id: string, name: string }> } };

export type GetCategoriesQueryVariables = Exact<{
  input: CategoriesInput;
}>;


export type GetCategoriesQuery = { __typename?: 'Query', categories: { __typename?: 'CategoryConnection', edges: Array<{ __typename?: 'CategoryEdge', cursor: string, node: { __typename?: 'Category', id: string, name: string, products: Array<{ __typename?: 'Product', id: string, name: string }> } }>, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, hasPreviousPage: boolean, endCursor?: string | null, hasNextPage: boolean } } };

export type GetCategoryQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetCategoryQuery = { __typename?: 'Query', category?: { __typename?: 'Category', id: string, name: string, products: Array<{ __typename?: 'Product', id: string, name: string }> } | null };

export type CreateProductMutationVariables = Exact<{
  input: CreateProductInput;
}>;


export type CreateProductMutation = { __typename?: 'Mutation', createProduct: { __typename?: 'Product', id: string, name: string, category: { __typename?: 'Category', id: string, name: string } } };

export type DeleteProductMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteProductMutation = { __typename?: 'Mutation', deleteProduct: { __typename?: 'Product', id: string, name: string, category: { __typename?: 'Category', id: string, name: string } } };

export type UpdateProductMutationVariables = Exact<{
  input: UpdateProductInput;
}>;


export type UpdateProductMutation = { __typename?: 'Mutation', updateProduct: { __typename?: 'Product', id: string, name: string, category: { __typename?: 'Category', id: string, name: string } } };

export type GetProductQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetProductQuery = { __typename?: 'Query', product?: { __typename?: 'Product', id: string, name: string, category: { __typename?: 'Category', id: string, name: string } } | null };

export type GetProductsQueryVariables = Exact<{
  input: ProductsInput;
}>;


export type GetProductsQuery = { __typename?: 'Query', products: { __typename?: 'ProductConnection', edges: Array<{ __typename?: 'ProductEdge', cursor: string, node: { __typename?: 'Product', id: string, name: string, category: { __typename?: 'Category', id: string, name: string } } }>, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, hasPreviousPage: boolean, endCursor?: string | null, hasNextPage: boolean } } };


export const CreateCategoryDocument = `
    mutation createCategory($input: CreateCategoryInput!) {
  createCategory(input: $input) {
    id
    name
    products {
      id
      name
    }
  }
}
    `;
export const DeleteCategoryDocument = `
    mutation deleteCategory($id: ID!) {
  deleteCategory(id: $id) {
    id
    name
    products {
      id
      name
    }
  }
}
    `;
export const UpdateCategoryDocument = `
    mutation updateCategory($input: UpdateCategoryInput!) {
  updateCategory(input: $input) {
    id
    name
    products {
      id
      name
    }
  }
}
    `;
export const GetCategoriesDocument = `
    query getCategories($input: CategoriesInput!) {
  categories(input: $input) {
    edges {
      node {
        id
        name
        products {
          id
          name
        }
      }
      cursor
    }
    pageInfo {
      startCursor
      hasPreviousPage
      endCursor
      hasNextPage
    }
  }
}
    `;
export const GetCategoryDocument = `
    query getCategory($id: ID!) {
  category(id: $id) {
    id
    name
    products {
      id
      name
    }
  }
}
    `;
export const CreateProductDocument = `
    mutation createProduct($input: CreateProductInput!) {
  createProduct(input: $input) {
    id
    name
    category {
      id
      name
    }
  }
}
    `;
export const DeleteProductDocument = `
    mutation deleteProduct($id: ID!) {
  deleteProduct(id: $id) {
    id
    name
    category {
      id
      name
    }
  }
}
    `;
export const UpdateProductDocument = `
    mutation updateProduct($input: UpdateProductInput!) {
  updateProduct(input: $input) {
    id
    name
    category {
      id
      name
    }
  }
}
    `;
export const GetProductDocument = `
    query getProduct($id: ID!) {
  product(id: $id) {
    id
    name
    category {
      id
      name
    }
  }
}
    `;
export const GetProductsDocument = `
    query getProducts($input: ProductsInput!) {
  products(input: $input) {
    edges {
      node {
        id
        name
        category {
          id
          name
        }
      }
      cursor
    }
    pageInfo {
      startCursor
      hasPreviousPage
      endCursor
      hasNextPage
    }
  }
}
    `;

const injectedRtkApi = api.injectEndpoints({
  endpoints: (build) => ({
    createCategory: build.mutation<CreateCategoryMutation, CreateCategoryMutationVariables>({
      query: (variables) => ({ document: CreateCategoryDocument, variables })
    }),
    deleteCategory: build.mutation<DeleteCategoryMutation, DeleteCategoryMutationVariables>({
      query: (variables) => ({ document: DeleteCategoryDocument, variables })
    }),
    updateCategory: build.mutation<UpdateCategoryMutation, UpdateCategoryMutationVariables>({
      query: (variables) => ({ document: UpdateCategoryDocument, variables })
    }),
    getCategories: build.query<GetCategoriesQuery, GetCategoriesQueryVariables>({
      query: (variables) => ({ document: GetCategoriesDocument, variables })
    }),
    getCategory: build.query<GetCategoryQuery, GetCategoryQueryVariables>({
      query: (variables) => ({ document: GetCategoryDocument, variables })
    }),
    createProduct: build.mutation<CreateProductMutation, CreateProductMutationVariables>({
      query: (variables) => ({ document: CreateProductDocument, variables })
    }),
    deleteProduct: build.mutation<DeleteProductMutation, DeleteProductMutationVariables>({
      query: (variables) => ({ document: DeleteProductDocument, variables })
    }),
    updateProduct: build.mutation<UpdateProductMutation, UpdateProductMutationVariables>({
      query: (variables) => ({ document: UpdateProductDocument, variables })
    }),
    getProduct: build.query<GetProductQuery, GetProductQueryVariables>({
      query: (variables) => ({ document: GetProductDocument, variables })
    }),
    getProducts: build.query<GetProductsQuery, GetProductsQueryVariables>({
      query: (variables) => ({ document: GetProductsDocument, variables })
    }),
  }),
});

export { injectedRtkApi as api };
export const { useCreateCategoryMutation, useDeleteCategoryMutation, useUpdateCategoryMutation, useGetCategoriesQuery, useLazyGetCategoriesQuery, useGetCategoryQuery, useLazyGetCategoryQuery, useCreateProductMutation, useDeleteProductMutation, useUpdateProductMutation, useGetProductQuery, useLazyGetProductQuery, useGetProductsQuery, useLazyGetProductsQuery } = injectedRtkApi;

